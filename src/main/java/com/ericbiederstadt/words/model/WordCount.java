package com.ericbiederstadt.words.model;

public class WordCount {
    public WordCount() {
    }

    public WordCount(String sentence, Integer wordCount) {
        this.sentence = sentence;
        this.wordCount = wordCount;
    }

    private String sentence;
    private Integer wordCount;

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public Integer getWordCount() {
        return wordCount;
    }

    public void setWordCount(Integer wordCount) {
        this.wordCount = wordCount;
    }
}
