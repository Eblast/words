package com.ericbiederstadt.words.model;

public class WordRequest {
    public WordRequest() {
    }

    public WordRequest(String phrase) {
        this.phrase = phrase;
    }

    private String phrase;

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }
}
