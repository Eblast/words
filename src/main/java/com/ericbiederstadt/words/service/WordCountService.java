package com.ericbiederstadt.words.service;

import com.ericbiederstadt.words.model.WordCount;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WordCountService {

    public List<WordCount> countWordsInPhrase(String phrase) {
        List<WordCount> wordCounts = new ArrayList<>();
        while(phrase.length() > 0) {
            int indexOfExclamation = phrase.indexOf('!') == -1 ? Integer.MAX_VALUE : phrase.indexOf('!');
            int indexOfQuestionMark = phrase.indexOf('?') == -1 ? Integer.MAX_VALUE : phrase.indexOf('?');
            int indexOfPeriod = phrase.indexOf('.') == -1 ? Integer.MAX_VALUE : phrase.indexOf('.');
            int firstPunctuationIndex = Math.min(indexOfExclamation, Math.min(indexOfQuestionMark, indexOfPeriod));
            String currentSentence = phrase.substring(0,firstPunctuationIndex + 1).trim();

            String sentenceWithNoPunctuation = currentSentence.replaceAll("[\\.?!]", "");
            String[] words = sentenceWithNoPunctuation.split(" ");
            wordCounts.add(new WordCount(currentSentence, words.length));

            phrase = phrase.substring(firstPunctuationIndex + 1);
        }

        return wordCounts;
    }
}
