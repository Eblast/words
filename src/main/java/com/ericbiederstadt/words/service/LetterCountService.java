package com.ericbiederstadt.words.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class LetterCountService {

    public Map<Character, Integer> countLettersInPhrase(String phrase) {
        char[] characterArray = phrase.toLowerCase().replaceAll("[^a-z]", "").toCharArray();

        Map<Character,Integer> charCounter = new HashMap<>();
        for(Character currentCharacter : characterArray) {
            if(charCounter.containsKey(currentCharacter)) {
                charCounter.put(currentCharacter, charCounter.get(currentCharacter) + 1);
            } else {
                charCounter.put(currentCharacter, 1);
            }
        }

        return charCounter;
    }
}
