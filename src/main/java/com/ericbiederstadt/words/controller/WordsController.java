package com.ericbiederstadt.words.controller;

import com.ericbiederstadt.words.model.WordCount;
import com.ericbiederstadt.words.model.WordRequest;
import com.ericbiederstadt.words.service.LetterCountService;
import com.ericbiederstadt.words.service.WordCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class WordsController {

    private WordCountService wordCountService;
    private LetterCountService letterCountService;

    @Autowired
    public WordsController(WordCountService wordCountService, LetterCountService letterCountService) {
        this.wordCountService = wordCountService;
        this.letterCountService = letterCountService;
    }

    @PostMapping(path="word_count_per_sentence")
    public List<WordCount> word_count_per_sentence(@RequestBody WordRequest request) {
        return wordCountService.countWordsInPhrase(request.getPhrase());
    }

    @PostMapping(path="total_letter_count")
    public Map<Character, Integer> total_letter_count(@RequestBody WordRequest request) {
        return letterCountService.countLettersInPhrase(request.getPhrase());
    }
}
