package com.ericbiederstadt.words.service;

import com.ericbiederstadt.words.model.WordCount;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class WordCountServiceTest {
    private WordCountService wordCountService;

    @Before
    public void setUp() {
        wordCountService = new WordCountService();
    }

    @Test
    public void countWordsInPhrase_separatesSentencesByQuestionMark_ExclamationPoint_andPeriod() {
        List<WordCount> wordCounts = wordCountService.countWordsInPhrase("Hello! How are you doing? I am fine.");

        assertEquals("Hello!", wordCounts.get(0).getSentence());
        assertEquals(1, wordCounts.get(0).getWordCount().intValue());
        assertEquals("How are you doing?", wordCounts.get(1).getSentence());
        assertEquals(4, wordCounts.get(1).getWordCount().intValue());
        assertEquals("I am fine.", wordCounts.get(2).getSentence());
        assertEquals(3, wordCounts.get(2).getWordCount().intValue());
    }

    @Test
    public void countWordsInPhrase_allowsNumbersInAWord() {
        List<WordCount> wordCounts = wordCountService.countWordsInPhrase("Is today the 24th?");

        assertEquals("Is today the 24th?", wordCounts.get(0).getSentence());
        assertEquals(4, wordCounts.get(0).getWordCount().intValue());
    }

    @Test
    public void countWordsInPhrase_allowsNonAlphaNumericCharactersInAWord() {
        List<WordCount> wordCounts = wordCountService.countWordsInPhrase("Hey, where is the Raleigh-Durham International Airport?");

        assertEquals("Hey, where is the Raleigh-Durham International Airport?", wordCounts.get(0).getSentence());
        assertEquals(7, wordCounts.get(0).getWordCount().intValue());
    }
}
