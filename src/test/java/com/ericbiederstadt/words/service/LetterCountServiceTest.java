package com.ericbiederstadt.words.service;

import org.junit.Before;
import org.junit.Test;
import java.util.Map;
import static org.junit.Assert.assertEquals;

public class LetterCountServiceTest {

    private LetterCountService letterCountService;

    @Before
    public void setUp() {
        letterCountService = new LetterCountService();
    }

    @Test
    public void countLettersInPhrase_returnsCountOfAlphabeticCharacters() {
        Map<Character, Integer> letterCounts = letterCountService.countLettersInPhrase("hey there");

        assertEquals(2, letterCounts.get('h').intValue());
        assertEquals(3, letterCounts.get('e').intValue());
        assertEquals(1, letterCounts.get('y').intValue());
        assertEquals(1, letterCounts.get('t').intValue());
        assertEquals(1, letterCounts.get('r').intValue());
        assertEquals(5, letterCounts.keySet().size());
    }

    @Test
    public void countLettersInPhrase_ignoresCase() {
        Map<Character, Integer> letterCounts = letterCountService.countLettersInPhrase("Hey There");

        assertEquals(2, letterCounts.get('h').intValue());
        assertEquals(3, letterCounts.get('e').intValue());
        assertEquals(1, letterCounts.get('y').intValue());
        assertEquals(1, letterCounts.get('t').intValue());
        assertEquals(1, letterCounts.get('r').intValue());
        assertEquals(5, letterCounts.keySet().size());
    }

    @Test
    public void countLettersInPhrase_ignoresNonAlphabeticCharacters() {
        Map<Character, Integer> letterCounts = letterCountService.countLettersInPhrase("!!!hey there?");

        assertEquals(2, letterCounts.get('h').intValue());
        assertEquals(3, letterCounts.get('e').intValue());
        assertEquals(1, letterCounts.get('y').intValue());
        assertEquals(1, letterCounts.get('t').intValue());
        assertEquals(1, letterCounts.get('r').intValue());
        assertEquals(5, letterCounts.keySet().size());
    }
}
