package com.ericbiederstadt.words.controller;

import com.ericbiederstadt.words.model.WordCount;
import com.ericbiederstadt.words.model.WordRequest;
import com.ericbiederstadt.words.service.LetterCountService;
import com.ericbiederstadt.words.service.WordCountService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class WordsControllerTest {
    private WordsController controller;
    private WordCountService mockWordCountService;
    private LetterCountService mockLetterCountService;

    @Before
    public void setUp() {
        mockWordCountService = Mockito.mock(WordCountService.class);
        mockLetterCountService = Mockito.mock(LetterCountService.class);
        controller = new WordsController(mockWordCountService, mockLetterCountService);
    }

    @Test
    public void word_count_per_sentence_usesPhraseFromPostBody() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

        mockMvc.perform(
            post("/word_count_per_sentence")
                .content(
                    "{\n" +
                    "  \"phrase\":\"something here\"\n" +
                    "}"
                )
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());

        verify(mockWordCountService).countWordsInPhrase("something here");
    }

    @Test
    public void total_letter_count_usesPhraseFromPostBody() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

        mockMvc.perform(
            post("/total_letter_count")
                .content(
                    "{\n" +
                        "  \"phrase\":\"something here\"\n" +
                        "}"
                )
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());

        verify(mockLetterCountService).countLettersInPhrase("something here");
    }

    @Test
    public void word_count_per_sentence_asksWordCountServiceToCountWords() {
        controller.word_count_per_sentence(new WordRequest("some sentence"));

        verify(mockWordCountService).countWordsInPhrase("some sentence");
    }

    @Test
    public void word_count_per_sentence_returnsWordCountFromWordCountService() {
        WordCount wordCount = new WordCount("Some sentence here", 3);
        WordCount otherWordCount = new WordCount("Other sentence here too", 4);
        List<WordCount> wordCounts = asList(wordCount, otherWordCount);
        when(mockWordCountService.countWordsInPhrase(any())).thenReturn(wordCounts);

        List<WordCount> actualWordCounts = controller.word_count_per_sentence(new WordRequest("some sentence"));

        assertEquals(actualWordCounts, wordCounts);
    }

    @Test
    public void total_letter_count_asksLetterCountServiceToCountLettersInPhrase() {
        controller.total_letter_count(new WordRequest("some sentence"));

        verify(mockLetterCountService).countLettersInPhrase("some sentence");
    }

    @Test
    public void total_letter_count_returnsTotalLetterCountFromLetterCountService() {
        Map<Character, Integer> letterCounts = new HashMap<>();
        letterCounts.put('A', 1);
        when(mockLetterCountService.countLettersInPhrase(any())).thenReturn(letterCounts);

        Map<Character, Integer> actualLetterCounts = controller.total_letter_count(new WordRequest("some sentence"));

        assertEquals(actualLetterCounts, letterCounts);
    }
}
